# design

## executing and rendering together

* saves cloning documents and elements
* less possibility for scripts to set headers or status code. There must be sufficient output stream buffering for that.
* [yellow] Scripts can influence document, but only the parts that have not been rendered yet. No need for an artificial variable "document", just use the same variables as are in the yellow script.

## tuning

* Comments are not rendered at all. Right decision?
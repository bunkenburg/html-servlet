# Òscar's comments:

## to do

* I started by the HTMLServlet and the Renderer. I think these classes are too big and could be broken down into smaller classes. Responsibilities are not very clear, and that makes it a bit difficult to follow the code. Also I am afraid that they are a bit coupled: HTMLServlet is calling Renderer and passes a reference of itself.

* Question: do you avoid to use libraries like Apache commons IOUtils or FileUtils on purpose? You could take advantage of it, i.e. to read files.

* In the tests, I saw you use the Mockito runner, but I didn't find any mocks.

* Suggestion: I would put the project in webapp form (or had a webapp module) so it's easier to deploy. Just adding the jetty plugin to start a server and deploy it, helps.

* Also, I would remove the eclipse .settings from bitbucket.

* Seeing the code I understand that you develop this servlet to be extended. There are a few protected methods. However I think the Open/Closed Principle could be enforced a bit. 

## done

* There is one thing that I would change, and that is the prelude initialization parameter. I think Javascript code doesn't belong to the web deployment descriptor. I would configure there the name of a script file or a Java class for the same purpose. I think it's not practical to have it there. WAIT! Reviewing the code I see you changed that already. Then you need to update it in the docs.

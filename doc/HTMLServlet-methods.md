# HTMLServlet methods

## JS

This could be a separate interface.

protected String getScriptEngineName(); --different named script engine
protected ScriptEngine getScriptEngine(); --different script engine
protected Bindings initialise(HttpServletRequest request, HttpServletResponse response); --more predefined variables
protected Throwable unwrap(Throwable e); --script engine specific unwrapping

## rendering

Separate interface?

protected Renderer newRenderer(Writer w); --modify rendering, e.g. inject js decorations on <input>

## file access

private String getPath(request) --Will this change?
protected Reader getReader(HttpServletRequest request); --Will probably not change, other than getPath(request) and getReader(path)
protected Reader getReader(String path); --read files from somewhere else

## html parsing

protected Document getDocument(String path); --parse(getReader(path))
protected Document parse(Reader reader); --maybe the only one that's needed.
protected DocumentBuilder getDocumentBuilder(); --choose document builder


# To-do html-servlet for 0.0.4

* multiple data-* attributes? Make tests.
* Consider separate interfaces: HTMLServlet-methods.md

## optimisation

* prelude: Its time should be the same as having a <script> in the page. Check.
* prelude: Can we cache the resulting state and re-use it? (Changes semantics. new Date())
* data-substitute: Its time should be just a little more than textual substitution. Check.
* The javascript evaluations are expensive for performance: optimise special cases like ${x}, ${x.y}, ${x.m()}. Investigate. Make a separate project for my optimised rhino. How long does one typical request take? How long for each step?
	-read template: on GAE 0ms
	-parse template: on GAE 2ms, local 10ms
	-first JS execution
	-each ${e} -- about 1ms

## with jetty

* add jetty for more realistic unit tests

* test that scripts can insert stuff into the writer
* Does Servlet spec API provide an include? Some unit tests to demonstrate it.

* error-page mechanism for 404 doesn't work. HTMLServlet sendError(404), but don't get 404.html. Investigate this in jetty.

* When scripts do resetBuffer(); w=getWriter(); w.close(), stop rendering.
* After forward, stop rendering.

## LATER

* Would be nice to have sessions as JS objects: session.x = session.x + 3
* consider predefined object "document", like in javascript in client. html5 spec
* consider response.getWriter().write(...) Where should that go? Disable
* $-expressions: escape "}" in "${expression}" somehow. 
	If you forget, expression will probably fail and render unevaluated. 
	In a javascript string, can use String.fromCharCode(125).
	Nothing if you want } as end of javascript literal object or block. 
* $-expressions: escape " ' somehow in expressions in attributes
	Use one or the other for nesting.
	In javascript strings:
	" String.fromCharCode(34)
	' String.fromCharCode(39)
* $-expressions: escape < in ${expression}?

# DONE

## 0.0.3

* Renderer.html(): boolean can be overridden.
* After response.sendRedirect(...), stop rendering, just empty the buffer.
* After response.sendError(..), stop rendering, just empty the buffer.

## 0.0.2

* better exceptions when JS fails: identifying document path and element XPath

## 0.0.1

* adds predefined objects pageContext and servletContext
* error page mechanism, like in Servlet Spec, with ${exception}
* prelude from a separate file
* merged executing and merging. No document cloning.
* bug: data-if="false", next text node is not rendered
* data-for accepts Iterable
* Just escapes ${E} results, for each location in document
* support redirect
* support POST
* Incorporate html-parser

## 0.0.0

* Liberate with proper license
* redirect: response.sendRedirect(String location)
* implement <script type="server/javascript" src="file.js"/>
* data-substitute with relative and absolute file path
* generalise over javascript engine
* data-for-x with arrays, collections, iterators
* Boolean html attributes
* Escape ${E} as \${E}, as in JSP (see section "1.2.2 Literal-expression" from the JSP 2.1 Expression Language Specification).

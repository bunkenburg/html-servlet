# html-servlet

HTMLServlet provides HTML templating with server-side javascript.

HTMLServlet is a servlet that is registered on *.html.

It reads the html file at the right location according to URL.
It renders the file as html, executing some scripting:

* ${expressions} in element bodies and attribute values
* data-* attributes for conditionals, loops, local variables, and inclusion
* &lt;script> element with server-side javascript
* boolean attributes.

Inspired by Apache Sightly (https://github.com/Adobe-Marketing-Cloud/sightly-spec),
but uses javascript as expression language and is independent of Sling.


## expressions

${E}

Syntax for the expression: javascript.

A ${expression} can appear in 

* element body
* attribute value.

It can not appear in tag name or as attribute key.

HTMLServlet evaluates the javascript expression and writes the result into the page.

Example:

	<!DOCTYPE html>
	<html>
		<head>
			<title>${person.name}</title>
		</head>
		<body>
			<h1 title="${person.name}">${person.name}</h1>
		</body>
	</html>


### escaping

The result of an ${E} expression will be escaped.

* Inside an element body, these characters will be escaped: & <.
* Inside an attribute value, these characters will be escaped: " '.
* Inside a cdata section, if the result contains ]]> then two CDATA sections will be rendered.
* Comments <!--bla--> are not rendered at all.
* In order to write ${E} without wanting it to be evaluated, escape $ by writing \${E}. The rendered page will contain ${E}.

Escape } inside the expression inside a javascript literal string by String.fromCharCode(125).
Escape } as end of a block: not implemented.

Inside an attribute value, the expression cannot contain ' or ", since that would break html parsing.
Inside an element, it cannot contain <, since that would break html parsing.


## data-* attributes

* data-for-x
* data-if
* data-substitute
* data-var-x

If an element has several of these attributes, they are evaluated in alphabetic order: 

0. for
1. if
2. substitute
3. var

### data-for-x

The last part of the attribute is taken as variable name.
The attribute value is a javascript expression.
The servlet evaluates the expression which should result in an array or something similar (java.util.Collection, java.lang.Iterable, java.util.Enumeration, java.util.Iterator).
The element will be repeated once for each value in the array which
will be available in the element.

Example:

	<ul>
		<li data-for-name="db.getNames()">Name: ${name}</li>
	</ul>
	
may result in

	<ul>
		<li>Name: Peter</li>
		<li>Name: Paul</li>
		<li>Name: Mary</li>
	</ul>


### data-if

The attribute value is a javascript expression. 
The servlet evaluates the expression.
If it gives true, the element is included.
If it gives false, the element is not included.

Example:

	<button data-if="user==null">login</button


### data-substitute

The attribute value is a path.
The servlet reads a file at the path. 
The file contains an HTML fragment.
The fragment is inserted in the document, substituting the original element.

There is some redundancy: the tag name of the original element makes no difference since it disappears.

Example:

	<div data-substitute="header.html"/>

The attribute value is literally a path. It is not a javascript expression which when
evaluated results in a path.

The path is relative to the location of the document in which it occurs,
or absolute if it starts with /.

### data-var-x

The last part of the attribute key is a variable name.
The attribute value is a javascript expression.
The servlet evaluates the expression and assigns the result to the variable,
for the scope of the element. After the element, the variable will have whatever value
it had before.

Example:

	<div data-var-name="user.getName()">Name: ${name}</div



## &lt;script>

	<script type="server/javascript">...</script>

The servlet executes the javascript on the server and then removes the element.

Predefined objects:

* request : HttpServletRequest
* response : HttpServletResponse
* session : HttpSession
* pageContext : javax.servlet.jsp.PageContext
* servletContext : javax.servlet.ServletContext


The included java objects and java classes can be called with the almost the same syntax as in java.

(See Java scripting documentation for details. http://docs.oracle.com/javase/7/docs/technotes/guides/scripting/programmer_guide/)

Example:

	<script type="server/javascript">
		importClass(Packages.cat.inspiracio.Person)
		var id=request.getParameter("id")
		var person=Person.lookup(id)
	</script>
		
The script program can also be in a separate file:

	<script type="server/javascript" src="functions.js" ></script>

In this case, the scripting is read from the file and the content of the script element is ignored.

The servlet accepts requests with methods GET and POST. The predefined object _request_
allows us to program something like this:

	<script type="server/javascript">
		if(request.getMethod()=="POST"){
			location=request.getParameter("location")
			response.sendRedirect(location)
		}
	</script>
	
As soon as the methods _response.sendRedirect()_ or _response.sendError()_ are called,
rendering is interrupted and the buffer is flushed. Nothing after these methods will be executed, and anything that was written to the response buffer beforehand is discarded.

## boolean attributes

An HTML-element attribute with value "false" is not rendered.
An attribute with _key_ and value "true" is rendered as _key_="_key_".

In that way, it is easy to script a boolean attribute:

	<input disabled="${E}">
	
assuming _E_ is a boolean expression. Its result will be _true_ or _false_
and the attribute will appear or not.


## Servlet configuration 

### parsing HTML

There are two servlets that you can use. They differ in how they 
parse HTML.

* cat.inspiracio.html.HTMLServlet. It uses Java's built-in XML parser to parse HTML. Your html must be good XML. It also outputs good XML which is acceptable for browsers. To use this class, you only need html-servlet.jar.
* cat.inspiracio.html.HTML5Servlet. It uses a real HTML5 parser. It also outputs real HTML5. To use this class, you also need the dependency "html-parser". Maven can do it for you.

### configuration

* servlet class: Decide which servlet class you need, "cat.inspiracio.html.HTMLServlet" or "cat.inspiracio.html.HTML5Servlet".

* mapping: Decide which URLs should be served by the servlet. The URL pattern "*.html" would be a normal choice, but you may include or exclude according to your needs.

* prelude: in an optional init-parameter "prelude" define a javascript program that is executed once at the start of each request.

* prelude-file: an optional init-parameter "prelude-file" with a path to a separate file that contains a javascript program that is executed once at the start of each request. The path is relative to /WEB-INF/web.xml. Example: "prelude.js" for /WEB-INF/prelude.js. The parameter "prelude-file" takes precedence over "prelude".

* script engine: in an optional init-parameter "javax.script.ScriptEngine" define the name of the script engine that you want. The default is "JavaScript".

* servlet-container: There's an optional init-parameter "servlet-container", to tell the html servlet which servlet container it is running in. Only used with value "GAE" for Google's App Engine, so that the servlet can implement a workaround for this bug: https://code.google.com/p/googleappengine/issues/detail?id=12906

Example:

	<servlet>
    	<servlet-name>html-servlet</servlet-name>
    	<servlet-class>cat.inspiracio.html.HTMLServlet</servlet-class>
		<init-param>
			<param-name>prelude</param-name>
			<param-value>
				importClass(Packages.cat.inspiracio.Person)
				id=request.getParameter("id")
				person=Person.lookup(id)
			</param-value>
		</init-param>
		<init-param>
			<param-name>javax.script.ScriptEngine</param-name>
			<param-value>rhino</param-value>
		</init-param>
	</servlet>
	<servlet-mapping>
    	<servlet-name>html-servlet</servlet-name>
    	<url-pattern>*.html</url-pattern>
	</servlet-mapping>


## error pages

You can define error pages in web.xml, just like described in the Servlet Spec 2.5.

For example, with these definitions:

	<error-page>
		<error-code>404</error-code>
		<location>/WEB-INF/404.html</location>
	</error-page>
	<error-page>
		<exception-type>java.lang.Throwable</exception-type>
		<location>/WEB-INF/throwable.html</location>
	</error-page>

If the html page calls response.sendError(404), the request will be forwarded to /WEB-INF/404.html.
If the html page throws an exception (any exception, since all exceptions are subclasses of java.lang.Throwable) in its javascript code, the request will be forwarded to /WEB-INF/throwable.html. 

If the defined error page is itself an html page, then in its execution the following request attributes and javascript variables will be available:

<table border="1">
	<tr>
		<th>request attribute
		<th>javascript variable
		<th>java type
		<th>what is it?
	<tr>
		<td>javax.servlet.error.status_code
		<td>status_code
		<td>java.lang.Integer
		<td>Status code of page that failed, maybe set by response.sendError(status)
	<tr>
		<td>javax.servlet.error.exception_type
		<td>exception_type
		<td>java.lang.Class
		<td>If the page that failed threw an exception, this it its class.
	<tr>
		<td>javax.servlet.error.message
		<td>message
		<td>java.lang.String
		<td>The exception message of the page that failed.
	<tr>
		<td>javax.servlet.error.exception
		<td>exception
		<td>java.lang.Throwable
		<td>If the page that failed threw an exception, this is it.
	<tr>
		<td>javax.servlet.error.request_uri
		<td>request_uri
		<td>java.lang.String
		<td>Request URI of the failed request.
	<tr>
		<td>javax.servlet.error.servlet_name
		<td>servlet_name
		<td>java.lang.String
		<td>Name of the servlet that failed, here, the name of HTMLServlet.
</table>

## extensions

You can inherited from cat.inspiracio.html.HTMLServlet and modify its behaviour. 
These are relevant protected methods:

* protected Bindings initialise(HttpServletRequest request, HttpServletResponse response) --if you want to use different initialised variables in javascript
* protected Reader getReader(String path) --if you want to retrieve html and js file from somewhere else
* protected Document parse(Reader reader) --if you want to use a different parser for html rather than Java's built-in XML DOM parser
* protected ScriptEngine getScriptEngine() --if you want to use a different scripting engine rather than the one specified in servlet init parameter "javax.script.ScriptEngine"


# security

From javascript we can call all Java classes, even "System.exit()".

# versions

## 0.0.2

* Better exceptions when javascript fails: identifying source file and element position

## 0.0.1

* Okay for Java 7 (Rhino) and Java 8 (Nashorn).
* prelude can be in separate file

## 0.0.0

Initial release.
/*

Copyright 2016 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.script.Bindings;
import javax.script.ScriptException;
import javax.script.SimpleBindings;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import cat.inspiracio.io.PrintStreamWriter;
import cat.inspiracio.servlet.http.InitialServletConfig;
import cat.inspiracio.servlet.http.InitialServletContext;
import cat.inspiracio.servlet.http.Session;
import cat.inspiracio.servlet.http.SetRequest;
import cat.inspiracio.servlet.http.SetResponse;
import cat.inspiracio.servlet.http.SetServletContext;

public abstract class AbstractTest {
	
	// state -----------------------------------------------
	
	protected ServletContext context=new InitialServletContext(){
		@Override public void log(String msg){AbstractTest.this.log(msg);}
		@Override public void log(Exception exception, String msg){log(msg + "\n" + exception);}
		@Override public void log(String message, Throwable throwable){log(message + "\n" + throwable);}

		/** @param path Starts with "/". */
		@Override public InputStream getResourceAsStream(String path){
			if(!path.startsWith("/"))
				throw new IllegalArgumentException(path);
			path=path.substring(1);//drop initial "/".
			return getInputStream(path);
		}
		
		@Override public String getContextPath(){return "";}
	};
	
	protected ServletConfig config=new InitialServletConfig(){
		@Override public String getServletName(){return "html-servlet";}
		@Override public ServletContext getServletContext(){return context;}
		@Override public String getInitParameter(String name){
			if("prelude".equals(name))
				return prelude;
			if("prelude-file".equals(name))
				return preludeFile;
			return null;
		}
		@Override public Enumeration<String> getInitParameterNames(){
			//return [] or ["prelude"]
			throw new RuntimeException();
		}
	};

	/** Contents of init parameter "prelude". */
	protected String prelude;
	
	/** Value of init parameter "prelude-file" */
	protected String preludeFile;
	
	protected HTMLServlet servlet;//must be instantiated by subclass
	protected Bindings bindings=new SimpleBindings();
	protected SetRequest request=new SetRequest();
	protected SetResponse response=new SetResponse();
	protected Session session=new Session();

	/** Text of the template (not path) */
	protected String template;

	// life cycle -----------------------------------------

	protected AbstractTest(){}
	
	protected void before(){
		try{
			servlet.init(config);
		}
		catch(ServletException e){
			throw new RuntimeException(e);
		}
		request.setSession(session);
		bindings.put("request", request);
	}

	protected void after(){}
	
	// helpers --------------------------------------------
	
	/** Gets a reader for a path,
	 * from a file in the classpath, including src/test/resources/. */
	protected Reader getReader(String file){
		try{
			Class<?> klass=getClass();
			ClassLoader loader=klass.getClassLoader();
			InputStream in=loader.getResourceAsStream(file);
			return new InputStreamReader(in, "UTF-8");
		}
		catch(UnsupportedEncodingException e){throw new RuntimeException("impossible");}
	}
	
	/** Gets an input stream for a path,
	 * from a file in the classpath, including src/test/resources/. 
	 * @param path */
	protected InputStream getInputStream(String path){
		Class<?>c=getClass();
		ClassLoader loader=c.getClassLoader();
		return loader.getResourceAsStream(path);
	}
	
	protected String getTitle(Document d){
		NodeList list=d.getElementsByTagName("title");
		if(list.getLength()==0)return null;
		Node title=list.item(0);
		return title.getTextContent();
	}

	protected void say(Object o){System.out.println("" + o);}
	
	protected List<Element> getChildElements(Node n){
		NodeList nodes=n.getChildNodes();
		List<Element> elements=new ArrayList<Element>();
		for(int i=0; i<nodes.getLength(); i++){
			Node node=nodes.item(i);
			if(node instanceof Element)
				elements.add((Element) node);
		}
		return elements;
	}

	/** Asserts that the response status is this. */
	protected void assertStatus(int status){
		int st=response.getStatus();
		assertEquals(status, st);
	}
	
	/** Asserts that the node is an element with this tag. */
	protected void assertTag(String tag, Node n){
		assertTrue(n instanceof Element);
		Element e=(Element)n;
		assertEquals(tag, e.getTagName());
	}
	
	/** Asserts that the node is a text with this text content. */
	protected void assertText(String tag, Node n){
		assertTrue(n instanceof Text);
		Text e=(Text)n;
		assertEquals(tag, e.getTextContent());
	}
	
	/** simulates the log file for the servlet */
	protected void log(String msg){
		say(msg);
	}

	/** Parses document from the response, after the request has been processed.
	 * 
	 * Correct behaviour: content-type="text/html" and response can be parsed as html.
	 * 
	 * If the content-type is not "text/html" or it is, but the response can not be parsed as
	 * html, throws NotHTMLException(response body).
	 * 
	 * @throws NotHTMLException
	 * */
	protected Document getDocument() throws NotHTMLException{
		
		String type=response.getContentType();
		
		if(type.startsWith("text/html")){
			try {
				Reader r=response.getReader();
				return servlet.parse(r);//IOException, SAXException
			}
			//Content-type is good, but returned text is not html: fall through
			catch (SAXException | IOException e){}
		}
		
		//not html: something is wrong. Fake an html document for convenience
		int status=response.getStatus();
		String message=response.getMessage();
		String collected=response.getBody();
		NotHTMLException ne=new NotHTMLException();
		ne.setStatusCode(status);
		ne.setMessage(message);
		ne.setContentType(type);
		ne.setText(collected);
		throw ne;
	}
	
	/** Make a readable html page for debugging.
	 * @param status code, like 500
	 * @param message from response
	 * @param text the response body 
	 * @return Document */
	private Document fake(int status, String message, String text){
		try {
			DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
			DocumentBuilder builder=factory.newDocumentBuilder();
			Document d=builder.newDocument();
			Element html=d.createElement("html");
			d.appendChild(html);
			
			Element head=d.createElement("head");
			html.appendChild(head);
			Element title=d.createElement("title");
			head.appendChild(title);
			title.setTextContent(status + " " + message);
			
			Element body=d.createElement("body");
			html.appendChild(body);
			Element pre=d.createElement("pre");
			body.appendChild(pre);
			pre.setTextContent(text);
			
			return d;
		}
		//never
		catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		}
	}
	
	/** Renders, using the current bindings. 
	 * @param file Does not start with "/". Relative to src/test/resources/. */
	protected Document render(String file) throws Exception{
		Reader r=getReader(file);
		Document in=servlet.parse(r);
		file="/"+ file;
		in.setDocumentURI(file);
		StringWriter w=new StringWriter();
		servlet.render(bindings, in, w);
		String s=w.toString();
		return parse5(s);
	}

    /** Renders a documents and returns the result as String. */
    protected String render(Document d) throws IOException, SAXException, ScriptException{
        StringWriter w=new StringWriter();
        servlet.render(bindings, d, w);
        return w.toString();
    }

    /** Simulates a GET request.
     * 
     * If HTMLServlet.doGet() throws exception, this method returns 500.
     * 
     * @param file Without initial "/". 
     * @throws ServletException Execution has failed.
     * @throws IOException Execution has failed.
     * @throws RuntimeException Execution has failed.
     * @throws NotHTMLException The response is not html.
     * */
	protected Document doGet(String file){
		//prepare
		file="/" + file;
		request.setRequestURI(file);
		request.setMethod("GET");
		
		//call servlet
		try{
			servlet.service(request, response);//ServletException, IOException, RuntimeException
			return getDocument();//NotHTMLException
		}
		catch(ServletException | IOException | RuntimeException | NotHTMLException e){
			e.printStackTrace();
			
			int status=500;
			String message=e.getMessage();
			String text=e.toString();
			response.setStatus(status);
			return fake(status, message, text);
		}
	}

	/** Puts a variable into the scripting bindings. */
	protected void put(String key, Object value){bindings.put(key, value);}

	protected String toString(Document d) throws Exception{
		StringWriter writer=new StringWriter();
		DocumentWriter dw=new DocumentWriter(writer);
		dw.document(d);
		return writer.toString();
	}

    /** Parses html into a document. 
     * @throws SAXException 
     * @throws IOException */
    protected Document parse(String s) throws IOException, SAXException{
        Reader reader=new StringReader(s);
        return servlet.parse(reader);
    }

    /** Parses html into a document. 
     * @throws SAXException 
     * @throws IOException */
    protected Document parse5(String s) throws IOException, SAXException{
        Reader reader=new StringReader(s);
        return parse5(reader);
    }

	/** Parses html5 into a document. 
	 * @throws SAXException 
	 * @throws IOException */
	protected Document parse5(Reader reader) throws IOException, SAXException{
		HTMLDocumentBuilder builder=new HTMLDocumentBuilder();
		InputSource source=new InputSource(reader);
		return builder.parse(source);
	}

	protected void say(Document d) throws Exception{
		Writer w=new PrintStreamWriter(System.out);
		DocumentWriter dw=new DocumentWriter(w);
		dw.document(d);
	}

	/** Simulates a GET request to this.template
	 * @throws IOException 
	 * @throws ServletException */
	protected void GET() throws ServletException, IOException {
		request.setMethod("GET");
		request.setRequestURI("template.html");//dummy
		context=new SetServletContext().setResource("template.html", template);
		servlet.doGet(request, response);
	}

	/** Asserts that the response body is a certain String. */
	protected void assertBody(String body) {
		String b=response.getBody();
		assertEquals(body, b);
	}

	/** Asserts that the response status message is a certain String. */
	protected void assertMessage(String message) {
		String m=response.getMessage();
		assertEquals(message, m);
	}

	protected void assertLocation(String location) {
		String loc=response.getHeader("Location");
		assertEquals(location, loc);
	}
	
}

/*

Copyright 2016 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/** Testing the HTML5Servlet. */
@RunWith(MockitoJUnitRunner.class)
public class ScriptTest extends AbstractTest{
	
	List<Long>times=new ArrayList<Long>();

	// life cycle --------------------------------------------
	
	public ScriptTest(){
		servlet=new HTML5Servlet();
	}
	
	@Before public void before(){super.before();}
	@After public void after(){super.after();}
	
	// Tests --------------------------------------------------
	
	@Test public void tFails() throws Exception{
		String file="cat/inspiracio/html/script/fails.html";
		String xpath="/html/body/script[@id=\"bad-script\"]";
		try{
			render(file);
			fail("Should have thrown exception.");
		}catch(ScriptException e){
			String f=e.getFileName();
			assertTrue(f, f.contains(file));
			assertTrue(f, f.contains(xpath));
		}
	}

	@Test public void tLess() throws Exception{
		String file="cat/inspiracio/html/script/less.html";
		Document d=render(file);
		Node script=d.getElementsByTagName("script").item(0);
		String s=script.getTextContent();
		assertEquals("x<y", s);
	}

}

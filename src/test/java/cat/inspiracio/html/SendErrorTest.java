/*

Copyright 2016 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SendErrorTest extends AbstractTest{

	// state --------------------------------------------	
	
	
	
	// life cycle --------------------------------------------
	
	public SendErrorTest(){
		servlet=new HTML5Servlet();
	}
	
	@Before public void before(){super.before();}
	@After public void after(){super.after();}
	
	// helpers ------------------------------------------------
	
	// Tests --------------------------------------------------
	
	@Test public void tsendRedirect()throws Exception{
		template=""
				+ "<html>"
				+ "	<head>"
				+ "		<script type='server/javascript'>"
				+ "			response.sendRedirect('http://www.google.com')"
				+ "		</script>"
				+ "	</head>"
				+ "	<body>Hola</body>"
				+ "</html";
		
		GET();
		assertStatus(302);//303 would be better
		assertLocation("http://www.google.com");
		assertBody("");
	}

	@Test public void tsendError401()throws Exception{
		template=""
				+ "<html>"
				+ "	<head>"
				+ "		<script type='server/javascript'>"
				+ "			response.sendError(401)"
				+ "		</script>"
				+ "	</head>"
				+ "	<body>Hola</body>"
				+ "</html";
		
		GET();
		assertStatus(401);
		assertBody("");
	}

	@Test public void tsendError401m()throws Exception{
		template=""
				+ "<html>"
				+ "	<head>"
				+ "		<script type='server/javascript'>"
				+ "			response.sendError(401, 'Unauthorized')"
				+ "		</script>"
				+ "	</head>"
				+ "	<body>Hola</body>"
				+ "</html";
		
		GET();
		assertStatus(401);
		assertMessage("Unauthorized");
		assertBody("");
	}

	@Test public void tsendError404m()throws Exception{
		template=""
				+ "<html>"
				+ "	<head>"
				+ "		<script type='server/javascript'>"
				+ "			response.sendError(404, 'Not Found')"
				+ "		</script>"
				+ "	</head>"
				+ "	<body>Hola</body>"
				+ "</html";
		
		GET();
		assertStatus(404);
		assertMessage("Not Found");
		assertBody("");
	}

	@Test public void tsendError500m()throws Exception{
		template=""
				+ "<html>"
				+ "	<head>"
				+ "		<script type='server/javascript'>"
				+ "			response.sendError(500, 'Server Error')"
				+ "		</script>"
				+ "	</head>"
				+ "	<body>Hola</body>"
				+ "</html";
		
		GET();
		assertStatus(500);
		assertMessage("Server Error");
		assertBody("");
	}

}

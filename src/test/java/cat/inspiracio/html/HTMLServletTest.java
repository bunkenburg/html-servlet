/*

Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.Writer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.script.ScriptException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import cat.inspiracio.io.PrintStreamWriter;
import cat.inspiracio.util.ArrayIterator;

@RunWith(MockitoJUnitRunner.class)
public class HTMLServletTest extends AbstractTest{

	// state --------------------------------------------
	
	// life cycle --------------------------------------------
	
	public HTMLServletTest(){
		servlet=new HTMLServlet();
	}
	
	@Before public void before(){super.before();}
	
	@After public void after(){super.after();}
	
	// Tests --------------------------------------------------
	
	@Test public void t0() throws Exception{
		prelude="surname='Bunkenburg'";
		request.setParameter("name", "Alexander");
		Document document=doGet("index.html");
		String title=getTitle(document);
		assertEquals("Alexander Bunkenburg", title);
	}
	
	@Test public void t01() throws Exception {
		put("title", "Alex");
		Document document=render("cat/inspiracio/html/exp/index.html");
		String title=getTitle(document);
		assertEquals(title, "Alex");
	}
	
	/** Testing whether URLs are rewritten according to base element. 
	 * @throws ScriptException */
	@Test public void tBase() throws Exception{
		Document document=doGet("cat/inspiracio/html/base/base.html");
		
		//Has the base element arrived?
		Element base=document.getElementById("base");
		String baseHref=base.getAttribute("href");
		assertEquals(baseHref, "/stories/alex/input/");
	}
	
	@Test public void tBoolean() throws Exception{
		Document document=doGet("cat/inspiracio/html/bool/boolean.html");
		Element e=document.getElementById("disabled");
		boolean b= e.hasAttribute("disabled");
		assertTrue(b);

		e=document.getElementById("enabled");
		b=e.hasAttribute("disabled");
		assertFalse(b);
	}

	@Test public void tArray() throws Exception{
		String[]array=new String[]{"one", "two", "three"};
		put("array", array);
		Document document=render("cat/inspiracio/html/for/array.html");
		for(String a : array){
			Element e=document.getElementById(a);
			String s=e.getTextContent();
			assertEquals(s, a);
		}
	}

	protected void say(Document d) throws Exception{
		Writer w=new PrintStreamWriter(System.out);
		DocumentWriter dw=new DocumentWriter(w);
		dw.document(d);
	}
	
	@Test public void tList() throws Exception{
		List<String>list=Arrays.asList("one", "two", "three");
		put("array", list);
		Document document=render("cat/inspiracio/html/for/array.html");
		for(String a : list){
			Element e=document.getElementById(a);
			String s=e.getTextContent();
			assertEquals(s, a);
		}
	}

	@Test public void tIterable() throws Exception{
		final List<String>list=Arrays.asList("one", "two", "three");
		Iterable<String>iterable=list;
		put("array", iterable);
		Document document=render("cat/inspiracio/html/for/array.html");
		for(String a : list){
			Element e=document.getElementById(a);
			String s=e.getTextContent();
			assertEquals(s, a);
		}
	}

	@Test public void tIterator() throws Exception{
		String[] array={"one", "two", "three"};
		Iterator<String> iterator=new ArrayIterator<String>(array);
		put("array", iterator);
		Document document=render("cat/inspiracio/html/for/array.html");
		for(String a : array){
			Element e=document.getElementById(a);
			String s=e.getTextContent();
			assertEquals(s, a);
		}
	}

	/** \${E} in element with escaping */
	@Test public void tElementEscape()throws Exception{
		Document document=doGet("cat/inspiracio/html/exp/escape.html");
		Element e=document.getElementById("escaped");
		String text=e.getTextContent();
		assertEquals("${escaped}", text);
	}
	
	/** \${E} in attribute value with escaping */
	@Test public void tAttributeEscape()throws Exception{
		Document document=doGet("cat/inspiracio/html/exp/escape.html");
		Element e=document.getElementById("escaped");
		String value=e.getAttribute("style");
		assertEquals("${escaped}", value);
	}

	/** Dangerous characters in element body.
	 * 
	 *  I'd like to test & < but they even make the 
	 *  parser fail so I cannot test them.
	 *  
	 *  It's correct that the parser fails.
	 * 
	 * HTMLServlet.doGet() throws ServletException,
	 * HTMLServletTest.doGet() must return 500.
	 * */
	@Test public void tescape()throws Exception{
		//Document document=
		doGet("cat/inspiracio/html/exp/bad.html");	
		assertStatus(500);
//		Element e=document.getElementById("not_escaped");
//		String value=e.getTextContent();
//		assertEquals(" & < ", value);
	}

	/** Dangerous characters in element attribute. */
	@Test public void tescapeAttribute()throws Exception{
		Document d=doGet("cat/inspiracio/html/exp/bad_attribute.html");	
		assertEquals(200, response.getStatus());
		Element e=d.getElementById("not_escaped");
		String style=e.getAttribute("style");
		assertEquals("'", style);
		String mark=e.getAttribute("mark");
		assertEquals("\"", mark);
	}

	/** ${E} in element */
	@Test public void tElementExpression()throws Exception{
		put("x", "Hello");
		Document document=render("cat/inspiracio/html/exp/expression.html");
		Element e=document.getElementById("x");
		String text=e.getTextContent();
		assertEquals("Hello", text);
	}

	/** Dangerous characters in element body,
	 * resulting from expression.
	 * */
	@Test public void tescape0()throws Exception{
		put("title", "&<");
		Document d=render("cat/inspiracio/html/exp/index.html");
		String title=getTitle(d);
		assertEquals("&<", title);
	}

	/** Dangerous characters in element attribute,
	 * resulting from expression.
	 * */
	@Test public void tescape1()throws Exception{
		put("x", "'");
		Document d=render("cat/inspiracio/html/exp/expression.html");
		Element x=d.getElementById("x");
		String style=x.getAttribute("style");
		assertEquals("'", style);
	}

	/** Dangerous characters in element attribute,
	 * resulting from expression.
	 * */
	@Test public void tescape2()throws Exception{
		put("x", "\"");
		Document d=render("cat/inspiracio/html/exp/expression.html");
		Element x=d.getElementById("x");
		String style=x.getAttribute("style");
		assertEquals("\"", style);
	}

	/** & in element attribute, resulting from expression, survives.
	 * It's necessary for composing URLs with parameters.
	 * */
	@Test public void tescape3()throws Exception{
		put("x", "&");
		Document d=render("cat/inspiracio/html/exp/expression.html");
		Element p=d.getElementById("x");
		String style=p.getAttribute("style");
		assertEquals("&", style);
	}

	/** < in element attribute,
	 * resulting from expression,
	 * survives (even though it doesn't seem useful).
	 * */
	@Test public void tescape4()throws Exception{
		put("x", "<");
		Document d=render("cat/inspiracio/html/exp/expression.html");
		Element x=d.getElementById("x");
		String style=x.getAttribute("style");
		assertEquals("<", style);
	}

	/** ]]> in cdata,
	 * resulting from expression,
	 * escaped.
	 * */
	@Test public void tescape5()throws Exception{
		put("x", "]]>");
		Document d=render("cat/inspiracio/html/exp/cdata.html");
		Element p=d.getElementById("escaped");
		String text=p.getTextContent();
		assertEquals("]]>", text);
	}

	/** --> in comment, resulting from expression: comment not rendered. */
	@Test public void tescape6()throws Exception{
        //Comments are dropped during parsing, so we must add them programmatically
        Document d=parse("<html><body><p id='p'/></body></html>");
        Element p=d.getElementById("p");
        Comment comment=d.createComment("comment");
        p.appendChild(comment);
        
        String s=render(d);

        assertFalse(s.contains("comment"));
        
        comment=d.createComment("${fail}");//not evaluated
        p.appendChild(comment);
        
        render(d);
        //reached here without exception
	}

	/** All escaping */
	@Test public void tescape7()throws Exception{
		Document document=doGet("cat/inspiracio/html/escape/escape.html");
		assertNotNull(document);
		assertEquals(200, response.getStatus());
		String body=response.getBody();
		assertTrue(body.contains("<script>&amp;&lt;></script>"));
		assertTrue(body.contains("<p style=\"'&quot;\">&amp;&lt;></p>"));
		assertTrue(body.contains("<div style=\"'&quot;\">&amp;&lt;></div>"));
	}

	/** ${E} in attribute value */
	@Test public void tAttributeExpression()throws Exception{
		put("x", "Hello");
		Document document=render("cat/inspiracio/html/exp/expression.html");
		Element e=document.getElementById("x");
		String style=e.getAttribute("style");
		assertEquals("Hello", style);
	}

	/** data-for-x with [] */
	@Test public void tForEmpty()throws Exception{
		int[] array={};
		put("array", array);
		Document document=render("cat/inspiracio/html/for/array0.html");
		Element e=document.getElementById("ol");
		NodeList children=e.getElementsByTagName("li");
		assertEquals(0, children.getLength());
	}

	/** data-for-x with [1, 2, 3] */
	@Test public void tForThree()throws Exception{
		int[] array={1, 2, 3};
		put("array", array);
		Document document=render("cat/inspiracio/html/for/array0.html");
		Element e=document.getElementById("ol");
		NodeList children=e.getElementsByTagName("li");
		assertEquals(3, children.getLength());
	}

	/** data-if true */
	@Test public void tIfTrue()throws Exception{
		Document document=doGet("cat/inspiracio/html/if/if.html");
		Element e=document.getElementById("yes");
		assertNotNull(e);
	}

	/** data-if false */
	@Test public void tIfFalse()throws Exception{
		Document d=doGet("cat/inspiracio/html/if/if.html");
		
		Element e=d.getElementById("no");
		assertNull(e);
		
		Element span=d.getElementById("span");
		assertNotNull(span);
		
		String s=toString(d);
		assertTrue(s.contains("bla"));
	}
	
	/** data-if some crap javascript value */
	@Test(expected=ScriptException.class)
	public void tIfCrap()throws Exception{
		Document document=render("cat/inspiracio/html/if/crap.html");
		document.getElementById("crap");
	}

	/** data-substitute */
	@Test public void tSubstitute()throws Exception{
		Document d=doGet("cat/inspiracio/html/substitute/dataSubstitute.html");
		String s=toString(d);
		Element body=(Element)d.getElementsByTagName("body").item(0);
		
		//one for one
		Element title=(Element)d.getElementsByTagName("title").item(0);
		s=title.getTextContent();
		assertEquals("The title!", s);
		
		//just text
		s=body.getTextContent();
		assertTrue(s.contains("The text"));
		
		//several
		List<Element>elements=getChildElements(body);
		assertEquals(4, elements.size());
		Element e=elements.get(0);
		assertEquals("h1", e.getTagName());
		assertEquals("Stays there", e.getTextContent());
		
		//one for one
		Element main=(Element)body.getElementsByTagName("main").item(0);
		elements=getChildElements(main);
		assertEquals(2, elements.size());
	}

	/** data-var-x with previous value */
	@Test public void tVar()throws Exception{
		put("x", "A");
		Document document=render("cat/inspiracio/html/var/var.html");
		
		Element div=document.getElementById("before");
		String text=div.getTextContent();
		assertEquals("A", text);
		
		div=document.getElementById("middle");
		text=div.getTextContent();
		assertEquals("B", text);
		
		div=document.getElementById("after");
		text=div.getTextContent();
		assertEquals("A", text);
	}

	/** script */
	@Test public void tScript()throws Exception{
		put("x", "A");
		Document document=render("cat/inspiracio/html/script/script.html");
		
		Element div=document.getElementById("before");
		String text=div.getTextContent();
		assertEquals("A", text);
		
		div=document.getElementById("after");
		text=div.getTextContent();
		assertEquals("B", text);
	}

	/** client-side script */
	@Test public void tScriptClient()throws Exception{
		Document document=doGet("cat/inspiracio/html/script/client.html");
		Element script=document.getElementById("script");
		String text=script.getTextContent();
		assertTrue(text, text.contains("var div='<div>&</div>'"));
	}

	/** script src */
	@Test public void tScriptSrc()throws Exception{
		put("x", "A");
		Document document=render("cat/inspiracio/html/script/src.html");
		
		Element div=document.getElementById("before");
		String text=div.getTextContent();
		assertEquals("A", text);
		
		div=document.getElementById("after");
		
		text=div.getTextContent();
		assertEquals("B", text);
	}

	/** prelude */
	@Test public void prelude()throws Exception{
		prelude="x='A'";
		Document document=doGet("cat/inspiracio/html/script/script.html");
		
		Element div=document.getElementById("before");
		String text=div.getTextContent();
		assertEquals("A", text);
		
		div=document.getElementById("after");
		text=div.getTextContent();
		assertEquals("B", text);
	}
	
	/** prelude-file */
	@Test public void preludeFile()throws Exception{
		preludeFile="prelude.js";
		//src/test/resources/WEB-INF/prelude.js: "x='A'"
		Document document=doGet("cat/inspiracio/html/script/script.html");
		
		Element div=document.getElementById("before");
		String text=div.getTextContent();
		assertEquals("A", text);
		
		div=document.getElementById("after");
		text=div.getTextContent();
		assertEquals("B", text);
	}
	
	/** response.sendRedirect(location) */
	@Test public void tRedirect()throws Exception{
		request.setRequestURI("/cat/inspiracio/html/get/redirect.html");
		
		servlet.service(request, response);
		
		int status=response.getStatus();
		String location=response.getHeader("Location");
		assertEquals(302, status);
		assertEquals("target.html", location);
	}

	@Test public void tPost()throws Exception{
		request.setRequestURI("/cat/inspiracio/html/post/post.html");
		request.setParameter("name", "Alex");

		servlet.doPost(request, response);
		
		int status=response.getStatus();
		assertEquals(200, status);
		
		Document d=getDocument();
		String title=getTitle(d);
		assertEquals("Alex", title);
	}

	@Test public void tPostRedirect()throws Exception{
		String location="http://www.google.com";
		request.setMethod("POST");
		request.setRequestURI("/cat/inspiracio/html/post/redirect.html");
		request.setParameter("location", location);

		servlet.service(request, response);
		
		int status=response.getStatus();
		assertEquals(302, status);
		String l=response.getHeader("Location");
		assertEquals(location, l);
	}
	
}

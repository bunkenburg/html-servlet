/*

Copyright 2016 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/** Testing the HTML5Servlet. */
@RunWith(MockitoJUnitRunner.class)
public class ForTest extends AbstractTest{
	
	List<Long>times=new ArrayList<Long>();

	// life cycle --------------------------------------------
	
	public ForTest(){
		servlet=new HTML5Servlet();
	}
	
	@Before public void before(){super.before();}
	@After public void after(){super.after();}
	
	// Tests --------------------------------------------------
	
	@Ignore
	@Test public void tFamily() throws Exception{
		List<String>array=strings(1000);
		put("cards", array);
		put("family", "Cactaceae");
		long start=System.currentTimeMillis();
		Document document=render("cat/inspiracio/html/for/familia.html");
		long end=System.currentTimeMillis();
		for(String a : array){
			Element e=document.getElementById(a);
			String s=e.getTextContent();
			assertEquals(s, a);
		}
		long delta=end-start;
		times.add(delta);
	}

	@Ignore
	@Test public void tFamilyN() throws Exception{
		int N=100;
		for(int i=0;i<N; i++)
			tFamily();
		say(times);
		say("first: " + times.get(0));
		times.remove(0);
		long sum=0;
		for(long t : times)
			sum+=t;
		say("avg: " + (sum/(N-1)));
	}
	
	List<String>strings(int n){
		List<String>list=new ArrayList<String>();
		for(int i=0; i<n; i++)
			list.add("s" + i);
		return list;
	}

	@Test public void tFails() throws Exception{
		String file="cat/inspiracio/html/for/fails.html";
		String xpath="/html/body/div[@id=\"${a}\"]/@data-for-a";
		try{
			render(file);
			fail("Should have thrown exception.");
		}catch(ScriptException e){
			String f=e.getFileName();
			assertTrue(f, f.contains(file));
			assertTrue(f, f.contains(xpath));
		}
	}

}

package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;

import org.junit.Test;

/** Analysing javascript engine compatibility problems between Java 7 with Rhino
 * and Java 8 with Nashorn. */
public class Java8Test {
    
    @Test public void t() throws ScriptException{
        Bindings bindings=new SimpleBindings();//javax.script.SimpleBindings@60d8c9b7
        
        //prelude()
        String prelude="surname='Bunkenburg'";
        ScriptEngineManager m=new ScriptEngineManager();
        String name="JavaScript";
        ScriptEngine e0=m.getEngineByName(name);
        //Java 7 puts all new variables into bindings, so on successive runs we must
        //use the same bindings, even with a different engine.
        //Java 8 puts the new variables into the engine somewhere, not into the 
        //bindings. Therefore we must use the same engine.
        e0.eval(prelude, bindings);
        
        //render()
        ScriptEngine e1=e0;//m.getEngineByName(name);//a new engine, Renderer.engine
        String expression="surname";
        Object r=e1.eval(expression, bindings);//same bindings, different engine. But surname is not in the bindings!
        
        assertEquals("Bunkenburg", r);
    }

}

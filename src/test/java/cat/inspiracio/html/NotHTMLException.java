package cat.inspiracio.html;

class NotHTMLException extends Exception {
	private static final long serialVersionUID = -3714059708494400193L;

	int status;
	String message;
	String type;
	String text;
	
	NotHTMLException(){}

	public void setStatusCode(int s){status=s;}
	public void setMessage(String m){message=m;}
	public void setText(String t){text=t;}
	public void setContentType(String t){type=t;}
}

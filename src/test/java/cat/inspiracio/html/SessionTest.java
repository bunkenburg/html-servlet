/*

Copyright 2016 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/** Testing the HTML5Servlet. */
@RunWith(MockitoJUnitRunner.class)
public class SessionTest extends AbstractTest{
	
	List<Long>times=new ArrayList<Long>();

	// life cycle --------------------------------------------
	
	public SessionTest(){
		servlet=new HTML5Servlet();
	}
	
	@Before public void before(){
		super.before();
		try{
			servlet.init(config);}
		catch(ServletException e){
			throw new RuntimeException(e);
		}
	}
	
	@After public void after(){super.after();}
	
	// Tests --------------------------------------------------
	
	@Test public void tSessionInTemplate() throws Exception{
		session.setAttribute("x", "Hola");
		Document d=doGet("cat/inspiracio/html/session/t0.html");
		Element e=d.getElementById("x");
		String text=e.getTextContent();
		assertEquals("Hola guapo", text);
	}

	@Test public void tSessionBetweenTemplates() throws Exception{
		session.setAttribute("x", "Hola");
		
		Document d=doGet("cat/inspiracio/html/session/t0.html");
		Element e=d.getElementById("x");
		String text=e.getTextContent();
		assertEquals("Hola guapo", text);
		String s=(String)session.getAttribute("x");
		assertEquals("Hola guapo", s);

		d=doGet("cat/inspiracio/html/session/t0.html");
		e=d.getElementById("x");
		text=e.getTextContent();
		assertEquals("Hola guapo guapo", text);
		s=(String)session.getAttribute("x");
		assertEquals("Hola guapo guapo", s);
	}

}

/*
Copyright 2015 Alexander Bunkenburg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio;

import java.util.Arrays;
import java.util.List;

public class Person {
	
	private String name;
	private String spouse;
	private List<String>children;

	public static Person lookup(String id){
		Person p=new Person();
		p.name="Maria";
		p.spouse="Pere";
		p.children=Arrays.asList("Anna", "Berta", "Clara");
		return p;
	}
	
	public String getName(){return name;}
	public boolean isMarried(){return spouse!=null;}
	public String getSpouse(){return spouse;}
	public List<String>getChildren(){return children;}
}

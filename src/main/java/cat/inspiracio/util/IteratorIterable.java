package cat.inspiracio.util;

import java.util.Iterator;

public class IteratorIterable<T> implements Iterable<T>{

	private Iterator<T>iterator;
	
	public IteratorIterable(Iterator<T> i){iterator=i;}
	
	@Override public Iterator<T> iterator(){return iterator;}
}

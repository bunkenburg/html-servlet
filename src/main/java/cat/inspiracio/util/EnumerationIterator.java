package cat.inspiracio.util;

import java.util.Enumeration;
import java.util.Iterator;

/** Wraps an old-fashioned enumeration into a modern iterator. */
public class EnumerationIterator<T> implements Iterator<T>{

	private Enumeration<T>e;
	
	public EnumerationIterator(Enumeration<T> e){this.e=e;}
	
	@Override public boolean hasNext(){return e.hasMoreElements();}
	@Override public T next(){return e.nextElement();}
	@Override public void remove(){throw new UnsupportedOperationException();}

}

package cat.inspiracio.util;

import java.lang.reflect.Array;
import java.util.Iterator;

public class ArrayIterator<T> implements Iterator<T>{

	private Object array;
	private int length;
	private int index=0;//next element to read
	
	public ArrayIterator(T[] a){
		array=a;
		length=Array.getLength(array);
	}
	
	/** 
	 * @param a the object which could be an array
	 * @param c Type of the array components */
	public ArrayIterator(Object a, Class<T>c){
		array=a;
		length=Array.getLength(array);
	}
	
	@Override public boolean hasNext(){return index<length;}
	@SuppressWarnings("unchecked")
	@Override public T next(){return (T) Array.get(array, index++);}
	@Override public void remove(){throw new UnsupportedOperationException();}
}

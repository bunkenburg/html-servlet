package cat.inspiracio.util;

import java.util.Iterator;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class IterableNodeList implements NodeList, Iterable<Node>{

	private NodeList nodes;
	
	public IterableNodeList(NodeList ns){nodes=ns;}
	
	@Override public Iterator<Node> iterator(){
		return new Iterator<Node>(){
			final int length=nodes.getLength();
			int index=0;
			@Override public boolean hasNext(){return index<length;}
			@Override public Node next(){return nodes.item(index++);}
			@Override public void remove(){throw new UnsupportedOperationException();}
		};
	}

	@Override public int getLength(){return nodes.getLength();}
	@Override public Node item(int arg0){return nodes.item(arg0);}

}

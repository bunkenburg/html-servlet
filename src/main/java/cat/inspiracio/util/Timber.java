package cat.inspiracio.util;

import java.util.HashMap;
import java.util.Map;

public class Timber {

	private static ThreadLocal<Timber> local=new ThreadLocal<Timber>(){
		@Override protected Timber initialValue(){return new Timber();}
	};

	private Map<String,Long>starts=new HashMap<String,Long>();
	private StringBuilder events=new StringBuilder();
	
	// public --------------------------------
	
	public static void start(String key){
		Timber t=local.get();
		t.starts.put(key, System.currentTimeMillis());
	}
	
	public static void stop(String key){
		Timber t=local.get();
		Long start=t.starts.remove(key);//may be null
		if(start==null)
			return;//No call to start(key) since last string()
		long delta=System.currentTimeMillis()-start;
		t.events.append(key).append(" ").append(delta).append("\n");
	}
	
	public static String string(){
		Timber t=local.get();
		String s=t.events.toString();
		t.events=new StringBuilder();
		t.starts.clear();
		return s;
	}
}

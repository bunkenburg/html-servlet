/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.servlet.jsp;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.jsp.JspWriter;

/** Wraps request.getWriter() : PrintWriter into JspWriter. Unfinished. */
class PrintJspWriter extends JspWriter {

	private static int BUFFER_SIZE = 1024;
	private static boolean AUTO_FLUSH = false;

	protected PrintJspWriter(PrintWriter print){super(BUFFER_SIZE, AUTO_FLUSH);}

	@Override public void clear() throws IOException{throw new RuntimeException();}
	@Override public void clearBuffer() throws IOException{throw new RuntimeException();}
	@Override public void close() throws IOException{throw new RuntimeException();}
	@Override public void flush() throws IOException{throw new RuntimeException();}
	@Override public int getRemaining(){throw new RuntimeException();}
	@Override public void newLine() throws IOException{throw new RuntimeException();}
	@Override public void print(boolean arg0) throws IOException{throw new RuntimeException();}
	@Override public void print(char arg0) throws IOException{throw new RuntimeException();}
	@Override public void print(int arg0) throws IOException{throw new RuntimeException();}
	@Override public void print(long arg0) throws IOException{throw new RuntimeException();}
	@Override public void print(float arg0) throws IOException{throw new RuntimeException();}
	@Override public void print(double arg0) throws IOException{throw new RuntimeException();}
	@Override public void print(char[] arg0) throws IOException{throw new RuntimeException();}
	@Override public void print(String arg0) throws IOException{throw new RuntimeException();}
	@Override public void print(Object arg0) throws IOException{throw new RuntimeException();}
	@Override public void println() throws IOException{throw new RuntimeException();}
	@Override public void println(boolean arg0) throws IOException{throw new RuntimeException();}
	@Override public void println(char arg0) throws IOException{throw new RuntimeException();}
	@Override public void println(int arg0) throws IOException{throw new RuntimeException();}
	@Override public void println(long arg0) throws IOException{throw new RuntimeException();}
	@Override public void println(float arg0) throws IOException{throw new RuntimeException();}
	@Override public void println(double arg0) throws IOException{throw new RuntimeException();}
	@Override public void println(char[] arg0) throws IOException{throw new RuntimeException();}
	@Override public void println(String arg0) throws IOException{throw new RuntimeException();}
	@Override public void println(Object arg0) throws IOException{throw new RuntimeException();}
	@Override public void write(char[] arg0, int arg1, int arg2) throws IOException{throw new RuntimeException();}
}

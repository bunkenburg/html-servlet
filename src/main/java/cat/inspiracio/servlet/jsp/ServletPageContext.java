/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.servlet.jsp;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;

/** Page context that's good for servlets. */
public class ServletPageContext extends InitialPageContext{

	/** Page context useful from within a servlet. 
	 * @param servlet the servlet
	 * @param request the request
	 * @param response the response */
	public ServletPageContext(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response){
		ServletConfig config=servlet.getServletConfig();
		ServletContext context=config.getServletContext();
		HttpSession session=request.getSession();
		setServlet(servlet);
		setRequest(request);
		setResponse(response);
		setSession(session);
		setServletConfig(config);
		setServletContext(context);
	}

	/** May call response.getWriter().
	 * @see cat.inspiracio.servlet.jsp.InitialPageContext#getOut() */
	@Override public JspWriter getOut() throws RuntimeException{
		try{
			JspWriter out=super.getOut();
			if(out==null){
				HttpServletResponse response=getResponse();
				PrintWriter print=response.getWriter();//IOException
				out=new PrintJspWriter(print);
				setOut(out);
			}
			return super.getOut();
		}
		catch(IOException e){throw new RuntimeException(e);}
	}

}

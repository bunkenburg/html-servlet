package cat.inspiracio.servlet.http;

/** An exception representing a http status code 4xx or 5xx.
 * 
 * I cannot use javax.xml.ws.http.HTTPException because Google Appengine 
 * restricts use of that class.
 * */
public class HTTPException extends RuntimeException {
	private static final long serialVersionUID = 5952534107126693347L;

	private int status;
	public HTTPException(int status){this.status=status;}
	public int getStatusCode(){return status;}
}

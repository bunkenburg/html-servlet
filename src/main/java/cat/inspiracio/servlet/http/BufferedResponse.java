package cat.inspiracio.servlet.http;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/** It seems that in appengine, the response buffer cannot be reset.
 * 
 * See http://stackoverflow.com/questions/36691900/httpservletresponse-resetbuffer-doesnt-work.
 * 
 * Until I find out what's wrong or find a fix, this is a workaround. 
 * 
 * This implementation is not complete nor correct:
 * at the end of doGet(), must call response.flushBuffer(),
 * setting and getting buffer size is not accurate.
 * */
public class BufferedResponse extends HttpServletResponseWrapper{

	// state -----------------------------------------------------
	
	/** If null, getWriter() has not been called yet, or it has been reset. */
	private PrintWriter writer;
	
	private int bufferSize=8192;
	
	// construction ---------------------------------------------
	
	public BufferedResponse(HttpServletResponse response){
		super(response);
	}

	// API methods ---------------------------------------------
	
	/** Puts a buffer around super.getWriter(). */
	@Override public PrintWriter getWriter() throws IOException{
		if(writer==null){
			Writer w=super.getWriter();
			w=new BufferedWriter(w, bufferSize);
			writer=new PrintWriter(w);
		}
		return writer;
	}

	@Override public void flushBuffer() throws IOException{
		if(writer!=null)writer.flush();
		super.flushBuffer();
	}

//	@Override public boolean isCommitted(){
//		/* Not so easy. We have to count the bytes that are written,
//		 * or make our own implementation of BufferedWriter. */
//	}
	
	@Override public void reset(){
		writer=null;
		super.reset();
	}

	@Override public void resetBuffer(){
		writer=null;	
		super.resetBuffer();
	}
	
}

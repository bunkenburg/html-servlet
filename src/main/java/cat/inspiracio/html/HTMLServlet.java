/*
Copyright 20ileNotFound15 Alexander Bunkenburg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static javax.script.ScriptEngine.FILENAME;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import cat.inspiracio.servlet.http.BufferedResponse;
import cat.inspiracio.servlet.http.HTTPException;
import cat.inspiracio.servlet.jsp.ServletPageContext;
import cat.inspiracio.util.Timber;

/** Receives GET requests for html files.
 * 
 * Parses the file, renders it (executing the data-attributes, the $-expressions, 
 * and the server-side script elements, and writes the resulting html document to
 * the response. 
 * 
 * For parsing, using Java's built-in XML-DOM parser, therefore the html file must be valid XML.
 * For parsing real HTML5 (rather than XML), use subclass HTML5Servlet.
 * 
 * The output is html5 and cannot be parsed as XML. */
public class HTMLServlet extends HttpServlet{
	private static final long serialVersionUID = -2680536696225745452L;

	// construction --------------------------------------------------

	public HTMLServlet(){}

	// business methods ----------------------------------------------

	/** Serves a request of method GET or POST. 
	 * @throws IOException Couldn't write to response
	 * @throws ServletException Something else, to show in error page
	 * */
	private void doIt(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		//Repairs reset()
		boolean gae=gae();
		if(gae)
			response=new BufferedResponse(response);
		
		Reader reader;
		try{
			Timber.start("getReader");
			reader=getReader(request);
			Timber.stop("getReader");
		}
		catch(FileNotFoundException e){
			response.sendError(404);
			return;
		}
		
		try{
			//sendError() and sendRedirect() abort rendering
			response=new HttpServletResponseWrapper(response){

				@Override public void sendError(int sc, String msg) throws IOException{
					super.sendError(sc, msg);
					throw new HTTPException(sc);
				}

				@Override public void sendError(int sc) throws IOException {
					super.sendError(sc);
					throw new HTTPException(sc);
				}

				@Override public void sendRedirect(String location) throws IOException {
					super.sendRedirect(location);
					throw new HTTPException(302);
				}
			};
			template(request, response, reader);
		}
		
		//html parsing failed
		catch (SAXException e){
			throw new ServletException(e);
		}

		//Javascript execution threw an exception
		catch (ScriptException e){
			
			//Thrown by response.sendError() to abort rendering
			HTTPException h=unwrapHTTPException(e);
			if(h!=null)
				response.resetBuffer();
			else
				//no unwrapping, so that error page can show line number
				throw new ServletException(e);
		}

		if(gae)
			response.flushBuffer();//BufferedResponse needs flush
	}
	
	/** Given an exception thrown by javascript execution, 
	 * unwrap a HTTPException from it, or null. */
	private HTTPException unwrapHTTPException(ScriptException e){
		Throwable t=e;
		
		while(true){
			
			//discriminate on t
			if(t==null)return null;
			if(t instanceof HTTPException)return (HTTPException)t;
			
			Throwable cause=t.getCause();
			if(t==cause)return null;

			t=cause;//go one deeper
		}
	}
	
	/** Sends a template.
	 * 
	 * If the template throws exception, sends corresponding error page.
	 * If the template sets error code, sends corresponding error page.
	 * 
	 * @param request
	 * @param response Assume that reset() works.
	 * @param reader Open reader to the template
	 * 
	 * @throws IOException Couldn't write to response
	 * @throws SAXException Couldn't parse html
	 * @throws ScriptException Javascript execution threw an exception
	 * */
	private void template(HttpServletRequest request, HttpServletResponse response, Reader reader)throws IOException, SAXException, ScriptException{
		if(request.getAttribute("javax.servlet.error.status_code")==null)
			response.setStatus(200);//initialise, maybe script will change it
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		Timber.start("parse");
		Document d=parse(reader);//Reading and parsing is fast. SAXException
		Timber.stop("parse");
		
		d.setDocumentURI(request.getRequestURI());
		Bindings bindings=initialise(request, response);
		PrintWriter w=response.getWriter();
		render(bindings, d, w);//ScriptException
	}

	/** Try to unwrap an exception thrown by the javascript engine.
	 * 
	 *  This method just returns the same exception,
	 *  respecting all abstract boundaries.
	 *  
	 *  Subclasses that know the javascript engine that is being used,
	 *  can really unwrap the exception. 
	 *  
	 * @param e the exception
	 * @return throwable
	 * */
	protected Throwable unwrap(Throwable e){return e;}
	
	/** Forwards the request.
	 * @throws IOException 
	 * @throws ServletException */
	@SuppressWarnings("unused")
	private void forward(HttpServletRequest request, HttpServletResponse response, String uri) throws ServletException, IOException{
		RequestDispatcher dispatcher=request.getRequestDispatcher(uri);
		dispatcher.forward(request, response);
	}
	
	@Override protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		long start=System.currentTimeMillis();
		String uri=request.getRequestURI();
		log("GET " + uri);
		doIt(request, response);
		long end=System.currentTimeMillis();
		long delta=end-start;
		log("HTMLServlet.doGet(" + uri + "): " + delta);
	}
	
	/** Receives a POST request and renders the html file
	 * which may send a redirect. */
	@Override protected void doPost(HttpServletRequest rq, HttpServletResponse rs)throws ServletException, IOException {
		String uri=rq.getRequestURI();
		log("POST " + uri);
		doIt(rq, rs);
	}

	// javascript -------------------------------------------

	/** Prepares the initial scripting state. 
	 * This implementation puts request, response, and session into it. 
	 * @param request the request
	 * @param response the response
	 * @return the bindings
	 * */
	protected Bindings initialise(HttpServletRequest request, HttpServletResponse response){
		Bindings bindings=new SimpleBindings();
		HttpSession session=request.getSession();
		ServletPageContext context=new ServletPageContext(this, request, response);
		ServletContext servletContext=getServletContext();
		bindings.put("request", request);
		bindings.put("response", response);
		bindings.put("session", session);
		bindings.put("pageContext", context);
		bindings.put("servletContext", servletContext);
		
		//If this is an error-page, there are request attributes
		bind(request, bindings, "status_code");
		bind(request, bindings, "exception_type");
		bind(request, bindings, "message");
		bind(request, bindings, "exception");//maybe this one is enough
		bind(request, bindings, "request_uri");
		bind(request, bindings, "servlet_name");
		
		return bindings;
	}
	
	private void bind(HttpServletRequest request, Bindings bindings, String name){
		Object o=request.getAttribute("javax.servlet.error." + name);
		if(o!=null)
			bindings.put(name, o);
	}

	/** Evaluate the prelude 
	 * @param engine The script engine to use. In Java 8 (Nashorn) we must 
	 * 	use the same engine instance for the whole page.
	 * @throws ScriptException 
	 * @throws FileNotFoundException The prelude file is not there. */
	private void prelude(ScriptEngine engine, Bindings bindings) throws ScriptException, FileNotFoundException{
		
		//prelude-file, file name relative to /WEB-INF/web.xml
		String preludeFile=getInitParameter("prelude-file");
		if(preludeFile!=null && 0<preludeFile.length()){
			Timber.start("prelude.js");
			String path="/WEB-INF/" + preludeFile;
			Reader reader=getReader(path);//FileNotFoundException
			engine.put(FILENAME, path);//This really is a file name.
			engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
			engine.eval(reader);
			Timber.stop("prelude.js");
			return;
		}
		
		//prelude directly in the init parameter
		String prelude=getInitParameter("prelude");
		if(prelude==null || 0==prelude.length())
		    return;
		
		engine.put(FILENAME, "prelude");
		engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
		engine.eval(prelude);
	}

	/** Executes prelude and renders the document.
	 * 
	 * Package-visibility for unit tests. 
	 * 
	 * @param bindings The scripting state to use
	 * @param in The parsed document
	 * @param w render the document to this output stream
	 * 
	 * @throws SAXException parsing
	 * @throws IOException Couldn't write to response
	 * @throws ScriptException JavaScript threw exception
	 * */
	void render(Bindings bindings, Document in, Writer w) throws IOException, SAXException, ScriptException{
	    //For Java 8 (Nashorn), only instantiate script engine once here. It will have state.
	    //It must be the same instance for the whole request.
	    ScriptEngine engine=getScriptEngine();
		prelude(engine, bindings);
		Renderer renderer=newRenderer(w);
		renderer.setServlet(this);
		renderer.setScriptEngine(engine);
		renderer.setBindings(bindings);
		try {
			Timber.start("render");
			renderer.document(in);
			Timber.stop("render");
			String s="<!--" + Timber.string() + "-->";
			w.append(s);
		}
		
		//improve exceptions

		//Acceptable as result of servlet.doGet()
		//Probably couldn't write to response
		catch(IOException e){
			throw e;
		}
		
		//Couldn't parse XML
		catch(SAXException e){
			throw e;
		}
		
		//Javascript execution threw an exception
		catch(ScriptException e){
			throw e;
		}
		
		//from redirect, maybe can avoid wrapping exception
		catch(HTTPException e){
			throw new ScriptException(e);
		}
		
		//Didn't expect this.
		catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	/** Instantiates a new Renderer. 
	 * Separate method so that subclasses can override and subclass the Renderer.
	 * @param w the writer
	 * @return a new renderer with that writer */
	protected Renderer newRenderer(Writer w){
		return new Renderer(w);
	}
	
	// parsing ----------------------------------

	/** Gets the html template that is requested, or FileNotFoundException.
	 * 
	 * Protected so that extension can decide how to map requests to html documents. 
	 * This implementation looks for a html file in the web app with path corresponding
	 * to the request URI.
	 * 
	 * @param request the request
	 * @return An open reader to the file or FileNotFoundException. 
	 * @throws FileNotFoundException Could not find the file corresponding to the request */
	protected Reader getReader(HttpServletRequest request)throws FileNotFoundException{
		String path=getPath(request);
		return getReader(path);
	}

	/** Gets the contents of a file, which may be an html or a js file,
	 * or FileNotFoundException. 
	 * 
	 * This implementation looks for a file in the web app.
	 * A subclass may override. 
	 * 
	 * @param path Absolute
	 * @return the reader 
	 * @throws FileNotFoundException Could not find the file for the path
	 * */
	protected Reader getReader(String path)throws FileNotFoundException{
		//Could consider caching here, or let the servlet container cache.
		try{
			ServletContext context=getServletContext();
			InputStream in=context.getResourceAsStream(path);
			if(in==null)
				throw new FileNotFoundException(path);//file not found
			return new InputStreamReader(in, "UTF-8");
		}
		catch(UnsupportedEncodingException e){throw new RuntimeException("impossible");}
	}

	/** Returns the path within the web app. */
	private String getPath(HttpServletRequest request){
		ServletContext context=getServletContext();
		String contextPath=context.getContextPath();
		String requestURI=request.getRequestURI();
		if(!requestURI.startsWith(contextPath))
			throw new RuntimeException("Request for " + requestURI + " went to wrong servlet");
		int length=contextPath.length();
		return requestURI.substring(length);//drop context path
	}

	/** Somehow gets the document from a path,
	 * and sets its document URI.
	 * 
	 * This implementation simply reads a file in the web root folder.
	 * Subclasses may override and do something else. 
	 * 
	 * @param path the path
	 * @return the document
	 * @throws FileNotFoundException Couldn't find the html file.
	 * @throws SAXException something wrong */
	protected Document getDocument(String path) throws IOException, SAXException{
		Reader reader=getReader(path);//FileNotFoundException
		Document d=parse(reader);
		d.setDocumentURI(path);
		return d;
	}

	/** Parses a document from a reader.
	 * 
	 * This implementation uses getDocumentBuilder().
	 * After parsing, it marks the id-attributes in the 
	 * document so that getElementById() works.
	 * 
	 * @param reader the reader 
	 * @return the document 
	 * @throws IOException something wrong
	 * @throws SAXException something wrong */
	protected Document parse(Reader reader) throws IOException, SAXException{
		DocumentBuilder builder=getDocumentBuilder();
		InputSource source=new InputSource(reader);
		Document d=builder.parse(source);
		return mark(d);
	}

	/** Gets the document builder to use in order to make html documents. 
	 * 
	 * This method returns Java's built-in XML DOM parser.
	 * @return the document builder */
	protected DocumentBuilder getDocumentBuilder(){
		try {
			DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
			return factory.newDocumentBuilder();
		}
		catch (ParserConfigurationException e){
			throw new RuntimeException();
		}
	}
	
	/** Marks attributes with key "id" as id-attributes. */
	private Document mark(Document d){
		if(d==null)return d;
		Element e=d.getDocumentElement();
		if(e==null)return d;
		mark(e);
		return d;
	}

	/** Marks attributes with key "id" as id-attributes. */
	private void mark(Element e){
		String id=e.getAttribute("id");
		if(id!=null && 0<id.length())
			e.setIdAttribute("id", true);
		
		NodeList children=e.getChildNodes();
		int length=children.getLength();
		for(int i=0; i<length; i++){
			Node child=children.item(i);
			if(child instanceof Element) mark((Element)child);
		}
	}
	
	// Script engine ---------------------------------------------
	
	/** Gets the script engine to be used for page rendering.
	 * This servlet returns Java's built-in javascript engine.
	 * @return the script engine */
	protected ScriptEngine getScriptEngine(){
		ScriptEngineManager manager=new ScriptEngineManager();
		String name=getScriptEngineName();
		return manager.getEngineByName(name);
	}
	
	/** Which script engine should we use?
	 * 
	 * This method read init parameter "javax.script.ScriptEngine",
	 * and returns "JavaScript" if there is no init parameter.
	 * 
	 * Subclasses may override. 
	 * @return the script engine name */
	protected String getScriptEngineName(){
		String name=getInitParameter("javax.script.ScriptEngine");
		if(name!=null)
			return name;
		return "JavaScript";
	}
	
	// helpers ---------------------------------------
	
	/** Running in Google App Engine? 
	 * (Needed to enable a workaround for GAE's lack of response.reset().)
	 * See http://stackoverflow.com/questions/36691900/httpservletresponse-resetbuffer-doesnt-work
	 * */
	private boolean gae(){
		String container=getInitParameter("servlet-container");
		if(container==null)
			return false;
		return "GAE".equals(container);
	}
}

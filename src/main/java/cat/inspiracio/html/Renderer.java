/*
Copyright 2016 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static javax.script.ScriptContext.ENGINE_SCOPE;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.NoSuchElementException;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import cat.inspiracio.util.ArrayIterator;
import cat.inspiracio.util.EnumerationIterator;
import cat.inspiracio.util.IteratorIterable;
import cat.inspiracio.util.Timber;

/** Renders a document, that is, visits all its elements and attributes,
 * executes data-* attributes, $-expressions, and script-elements,
 * and sends the resulting characters to a writer. */
public class Renderer extends DocumentWriter {
	
	// constants -----------------------------------------------------
	
	private static final String DATA_FOR="data-for-";
	private static final String DATA_IF="data-if";
	private static final String DATA_SUBSTITUTE="data-substitute";
	private static final String DATA_VAR="data-var-";

	// state -----------------------------------------

	/** We'll need Document parse(path). */
	private HTMLServlet servlet;
	
	/** The javascript state */
	private Bindings bindings;

	/** The javascript engine */
	private ScriptEngine engine;

	// construction ----------------------------------

	protected Renderer(Writer w){super(w);}
	
	/** 
	 * @param s The HTMLServlet
	 * @param e The script engine to use.
	 *     In Java 8 (Nashorn), it must be the same instance for the whole request. 
	 * @param w Writes the output to this writer. */
	public Renderer(HTMLServlet s, ScriptEngine e, Writer w){
		super(w);
		servlet=s;
		engine=e;
	}
	
    protected void setBindings(Bindings b){bindings=b;}

    /** Sets the script engine to use. 
     * In Java 8 (Nashorn), it must be the same instance for the whole request.
     * @param e The script engine to use. */
	protected void setScriptEngine(ScriptEngine e){engine=e;}
	
	protected void setServlet(HTMLServlet s){servlet=s;}
	
	// business methods ------------------------------
	
	/** Logs a message, via the servlet. 
	 * @param msg the message */
	protected void log(Object msg){
	    if(msg!=null)
	        servlet.log(msg.toString());
	}
	
	// Javascript evaluation -----------------------------------------
	
	/** Evaluates a javascript expression, by calling evaluate(filename, expression).
	 * @param expression the expression
	 * @return the result 
	 * @throws ScriptException something wrong
	 * */
	@Deprecated
	protected Object evaluate(String expression)throws ScriptException{
		//String source=shorten(expression);
		Node source=null;
		return evaluate(source, expression);
	}
	
	/** Override this method in order to change all javascript evaluation,
	 * including the methods evaluate*.
	 * 
	 * @param source For debug, an indication of where the expression is,
	 * 	like file name of source file and XPath expression of element,
	 * 	or line and character.
	 * @param expression the expression
	 * @return result
	 * @throws ScriptException something wrong 
	 * */
	protected Object evaluate(Node source, String expression)throws ScriptException{
		try{
			engine.setBindings(bindings, ENGINE_SCOPE);
			return engine.eval(expression);
		}
		catch(ScriptException e){
			//Loses e.getCause(). Maybe I can construct a better new ScriptException.
			String message=e.getMessage();
			String loc=getLocation(source);
			int lineNumber=e.getLineNumber();
			int columnNumber=e.getColumnNumber();
			throw new ScriptException(message, loc, lineNumber, columnNumber);
		}
	}
	
	/** Receives a javascript expression or program,
	 * and shortens it to a few characters.
	 * The result is useful just for identifying what
	 * is executing. */
	@SuppressWarnings("unused")
	private String shorten(String expression){
		if(expression==null)return "null expression";
		expression=expression.trim();
		int length=expression.length();
		final int MAX=20;
		if(MAX<length)
			return "expression: " + expression.substring(0, MAX) + "...";
		return "expression: " + expression;
	}

	/** Calls evaluate(String) and converts the result to an iterable. 
	 * @param source an indication where the expression is, for debug.
	 * 	Good values: file name of source file and XPath of the element that has the expression.
	 * @param expression the expression
	 * @return iterable result
	 * @throws ScriptException something wrong
	 * @throws NoSuchElementException something wrong 
	 * */
	private Iterable<?> evaluateIterable(Node source, String expression)throws ScriptException{
		Object result=evaluate(source, expression);
		return toIterable(source, result);
	}

	/** Calls evaluate(String) and converts the result to boolean.
	 * @param location For debug, indication of where the expression is
	 * @param expression 
	 * @return result
	 * @throws ScriptException 
	 * @throws NoSuchElementException */
	private boolean evaluateBoolean(Node source, String expression)throws ScriptException{
		Object result=evaluate(source, expression);
		return toBoolean(source, result);
	}

	/** Calls evaluate(String) and then converts result to String. 
	 * Returns a String, never returns null.
	 * @param source source node
	 * @param expression the expression
	 * @return resulting string
	 * @throws ScriptException 
	 * */
	private String evaluateString(Node source, String expression)throws ScriptException{
		Object result=evaluate(source, expression);
		return toString(result);
	}

	/** Interprets result of javascript execution as java.lang.Iterable.
	 * Accepts arrays, collections, iterators, enumerations, and iterables.
	 * This method does not pull elements out of iterators and similar;
	 * it only wraps them. 
	 * @param location Where is the expression? For debug.
	 * @return iterable result
	 * @throws ScriptException The result can not be interpreted as iterable. 
	 * @return Maybe null. */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Iterable<?> toIterable(Node source, Object result)throws ScriptException{
		if(result==null)
			return null;
		
		//Iterable, covers Collections
		if(result instanceof Iterable)
			return (Iterable)result;

		//Enumeration
		if(result instanceof Enumeration){
			Enumeration<?>e=(Enumeration<?>)result;
			result=new EnumerationIterator(e);
			//later branch will treat result
		}
		
		//array
		Class<?> c=result.getClass();
		if(c.isArray()){
			Class<?>type=c.getComponentType();
			result=new ArrayIterator(result, type);
			//later branch will treat result
		}
		
		//Iterator
		if(result instanceof Iterator){
			Iterator<?>iterator=(Iterator<?>)result;
			return new IteratorIterable(iterator);
		}

		String message="Not an iterable: " + result + ": " + c;
		String location=getLocation(source);
		throw new ScriptException(message, location, -1);
    }

	/** Interprets a result of javascript execution as boolean. 
	 * Accepts only real booleans, does not map other types to boolean
	 * the way javascript does. 
	 * @param source Location of the expression
	 * @param result
	 * @return boolean result
	 * @throws ScriptException The result is not a boolean. */
    private boolean toBoolean(Node source, Object result)throws ScriptException{
    	if(result instanceof Boolean)
    		return (Boolean)result;
		String message="Not a boolean: " + result;
		String location=getLocation(source);
		throw new ScriptException(message, location, -1);
    }
    
    /** Interprets a result of javascript execution as String. Maps null to "". 
     * Never returns null. */
	private String toString(Object result){
		if(result==null)
			return "";
		String s=result.toString();//bad toString() method could still give null
		if(s==null)
			return "";
		return s;
	}

	// rendering ---------------------------------------
	
	/** HTML template language rendering
	 * @param e Element to treat in the recursion
	 * @return the document writer */
	@Override protected DocumentWriter element(Element e)throws Exception{
	
		if(isServerScript(e)){
			scriptServer(e);
			return this;
		}
		
		// find data-* attributes
		Attr dataFor=null;
		Attr dataIf=null;
		Attr dataVar=null;
		Attr dataSubstitute=null;
		NamedNodeMap attributes=e.getAttributes();
		//Loop over attributes because of prefix-match for data-for-x and data-var-x
		for(int i=0; i<attributes.getLength(); i++){
			Attr a=(Attr)attributes.item(i);
			String name=a.getName();
			if(name.equals(DATA_IF))dataIf=a;
			if(name.startsWith(DATA_FOR))dataFor=a;
			if(name.startsWith(DATA_VAR))dataVar=a;
			if(name.equals(DATA_SUBSTITUTE))dataSubstitute=a;
		}
		
		
		if(dataFor!=null){
            renderFor(e, dataFor);//ScriptException
            return this;//We've already recursed.
        }
        
		if(dataIf!=null){
			renderIf(e, dataIf);
			return this;//We've already recursed.
		}
		
        if(dataVar!=null){
        	renderVar(e, dataVar);
        	return this;//We've already recursed.
        }
		
		if(dataSubstitute!=null){
			renderSubstitute(e, dataSubstitute);
			return this;//We've already recursed.
		}
		
        if(isScript(e)){
        	scriptClient(e);
        	return this;
        }

        return super.element(e);
	}

	/** Render a client-side script element. 
	 * In such an element, expect only these children: text, cdata, comment.
	 * 
	 * For HTML5:
	 * The Text children will be rendered without escaping,
	 * since they contain javascript code. 
	 * Here, allow javascript injection, which is the 
	 * point of a script element.
	 * 
	 * For HTML4:
	 * Inside Text children, escape & < > by their entities.
	 * 
	 * @param e the script element
	 * 
	 * @throws Exception */
	private void scriptClient(Element e) throws Exception{
		open(e);
		
		NodeList children=e.getChildNodes();
		final int length=children.getLength();
		for(int i=0; i<length; i++){
			Node child=children.item(i);

			if(child instanceof CDATASection)
				cdata((CDATASection)child);
			
			else if(child instanceof Comment)
				comment((Comment)child);
			
			else if(child instanceof Text){
				Text text=(Text)child;
				String t=text.getData();
				String s=render(text, t);
				
				//HTML5: no escaping at all
				if(html5())
					write(s);
				//HTML4: escape & < >
				else
					writeEscapeElement(s);
			}
			
			else{
				String msg="Not expecting " + child + " in script element " + getLocation(e);
				throw new RuntimeException(msg);
			}
		}
		close(e);
	}
	
	/** brittle test whether we want html5
	 * @return Is this html5?  */
	protected boolean html5(){return servlet instanceof HTML5Servlet;}    

	/** Executes the javascript in a server-side script element. 
	 * @throws ScriptException Execution of script throws exception
	 * @throws IOException Script is in separate file which couldn't be read */
	private void scriptServer(Element script) throws ScriptException, IOException{
		Timber.start("script");
		String program=getScript(script);
		evaluate(script, program);
		Timber.stop("script");
	}
	
	private String xpath(Element e){
		//This method is not so efficient and should only be called in case of exception.
		
		//mine:
		//	"div[@id=x13]"	if element has id
		//	"div[12]"		if there are many div in the parent
		//	"div"			if there is only one div in the parent
		String tag=e.getTagName();
		String mine=tag;
		if(e.hasAttribute("id"))
			mine += "[@id=\"" + e.getAttribute("id") + "\"]";
		else{
			Node n=e.getParentNode();
			if(n!=null && n instanceof Element){
				Element p=(Element)n;
				NodeList es=p.getElementsByTagName(tag);
				if(1<es.getLength()){
					for(int i=0; i<es.getLength(); i++){
						if(e==es.item(i)){
							mine += "[" + (i+1) + "]";
							break;//out of for-loop
						}
					}
				}
			}
		}
		
		//parent:
		//	"/html"
		//	"/html/body/div"	
		String parent="";
		Node n=e.getParentNode();
		//Improve: remove the recursion.
		if(n!=null && n instanceof Element){
			Element p=(Element)n;
			if(p!=null)
				parent=xpath(p);
		}
		
		return parent + "/" + mine;
	}
	
	private boolean empty(String s){return s==null || 0==s.length();}
	
	/** From a script element, gets the script program.
	 * It may be in a separate file (indicated by relative path in
	 * src attribute) or it may be the text content of the element. 
	 * @throws IOException Reader separate file failed. */
	private String getScript(Element script) throws IOException{
		//Not so efficient. Could return a Reader in case it's a separate file.
		//See whether there's a src attribute that points to a separate file.
		String src=script.getAttribute("src");
		if(src!=null && 0<src.length()){
			Document d=script.getOwnerDocument();
			String uri=d.getDocumentURI();
			src=resolve(uri, src);
			Reader reader=getReader(src);
			return collect(reader);
		}
		
		return script.getTextContent();
	}
	
	/** Gets a reader for a path.
	 * @param path The path is absolute.
	 * @return the reader 
	 * @throws IOException something wrong 
	 * @throws FileNotFoundException The file is not there.
	 * */
	protected Reader getReader(String path) throws IOException{
		return servlet.getReader(path);
	}

	/** Collects the characters from a reader. */
	private String collect(Reader reader) throws IOException{
		if(reader==null)
		    return "";
		if(!(reader instanceof BufferedReader))
		    reader=new BufferedReader(reader);
		StringBuilder builder=new StringBuilder();
		// optimise?
		int i=reader.read();
		while(0<=i){
			char c=(char)i;
			builder.append(c);
			i=reader.read();
		}
		return builder.toString();
	}
	
	/** Is e a script element? */
	private boolean isScript(Element e){
		String tag=e.getTagName();
		return "script".equalsIgnoreCase(tag);
	}

	/** Is e a server-side script?
	 * Looking for 
	 * &lt;script type="server/javascript">...&lt;/script> */
	private boolean isServerScript(Element e){
		String type=e.getAttribute("type");
		if(empty(type))
			return false;
		String tag=e.getTagName();
		return "server/javascript".equals(type) && "script".equalsIgnoreCase(tag);
	}
	
	/** Substitutes ${E} in attribute values.
	 * @param a the attribute
	 * @return the document writer 
	 * @throws IOException something wrong */
	@Override protected DocumentWriter attribute(final Attr a)throws Exception{
		String key=a.getName();
		String value=a.getValue();
		value=render(a, value);//ScriptException

		//true attribute: key=key
		if("true".equals(value))
			value=key;

		//false attribute disappears
		else if("false".equals(value))
			return this;

		return attribute(key, value);
	}

	/** Substitutes ${E} in the text.
	 * 
	 * Any expression will be evaluated and its result will
	 * be escaped for safe inclusion in an element body.
	 * @param text the text
	 * @return the document writer
	 * @throws Exception something wrong
	 * */
	@Override protected DocumentWriter text(final Text text) throws Exception{
		String t=text.getData();
		String s=render(text, t);
		return text(s);
	}

	/** Does not render comments at all.
	 * @param comment the ignored comment
	 * @return the document writer
	 * */
	@Override protected Renderer comment(Comment comment){return this;}
	
	/** Substitutes ${E} inside the cdata section.
	 * @param cd the CData
	 * @return the document writer
	 * @throws Exception something wrong
	 * */
	@Override protected DocumentWriter cdata(CDATASection cd) throws Exception{
		String t=cd.getData();
		String s=render(cd, t);
		return cdata(s);
	}

    /** Substitutes any ${expression} in the string by its value. 
	 * Escape "${" by writing "\${".
	 * @param source Location of the expression
	 * @param s the string that may contain expressions
	 * @return the resulting string
	 * @throws IOException something wrong
	 * @throws ScriptException something wrong */
	protected String render(Node source, String s)throws IOException, ScriptException{
		//There is string construction here. Can we remove it?
		//At least in the usual case (no ${E}), it is fine.
		StringBuilder builder=null;
		
		int start=s.indexOf("${");
		while(0<=start){
			//There will be change. Work on builder instead of s.
			//Initialise builder only in unusual case, here, inside loop.
			if(builder==null)
				builder=new StringBuilder(s);
			
			//check that it's not \${...}
			boolean escaped = 1<=start && builder.charAt(start-1)=='\\';
			
			if(escaped){
				//Must output $ for \$
				
				// builder[start-1] = \
				// builder[start]   = $
				// builder[start+1] = {
				
				//delete \
				//s = s.substring(0, start-1) + s.substring(start);
				builder.deleteCharAt(start-1);
				
				// look for the next ${
				start=builder.indexOf("${", start);
			} 
			
			else if(!escaped){

				int end=builder.indexOf("}", start);
				if(-1==end){
					String location=getLocation(source);
					String message="Unterminated expression: " + builder + " at " + location;
					throw new IOException(message);
				}
				String expression=builder.substring(start+2, end);//new String
				String t=evaluateString(source, expression);//ScriptException
				
				//substitute builder[start, end+1] by t
				//s=s.substring(0, start) + t + s.substring(end+1);
				builder.replace(start, end+1, t);
				start=builder.indexOf("${", start + t.length());
				
			}
		}
		//In the usual case, never instantiated builder.
		if(builder!=null)
			return builder.toString();
		return s;
	}
	
    /** Resolves data-for- attribute. 
	 * Already recurses into the resulting elements.
	 * @param e the element
	 * @param dataFor the element with the data-for- attribute 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ScriptException */
	private void renderFor(Element e, Attr dataFor)throws Exception{
        String name=dataFor.getName();
        String var=name.substring(DATA_FOR.length(), name.length());
        String value=dataFor.getValue();
        Iterable<?> iterable=evaluateIterable(dataFor, value);
        if(iterable==null){
            //or just return
        	String location=getLocation(dataFor);
        	throw new NullPointerException("data-for delivers null at " + location);
        }
        boolean b=contains(var);
        Object oldX=get(var);
        e.removeAttribute(name);//Must remove the attribute. Put it back after.

        for(Object x : iterable){
            put(var, x);//Put "var" -> x into the scope
            element(e);//will mark the id
        }
        
        if(b)put(var, oldX);else remove(var);
        
        e.setAttribute(name, value);//put data-for back, so that we leave document unchanged
	}

	/** For debug, gets the xpath that selects an attribute. 
	 * Call only in case of exception.
	 * @param a the attribute 
	 * @return the location as XPath */
	protected String getLocation(Attr a){
		Element e=a.getOwnerElement();
		String location=getLocation(e);
		return location + "/@" + a.getName();
	}

	/** For debug, XPath for the element that contains the text.
	 * Call only in case of exception. 
	 * @param e */
	private String getLocation(Text t){
		Node parent=t.getParentNode();
		while(parent!=null && parent.getNodeType()!=Node.ELEMENT_NODE)
			parent=parent.getParentNode();
		if(parent==null)return "";
		Element e=(Element)parent;
		return getLocation(e);
	}

	/** For debug, gets the XPath for the element.
	 * Call only in case of exception.  
	 * @param e */
	private String getLocation(Element e){
		// This must go somewhere else.
//		if(isServerScript(e)){
//			String src=e.getAttribute("src");
//			if(!empty(src))
//				return src;
//		}

        String location=xpath(e);
        //This is the xpath inside the document. But where does the document come from?
        //Maybe it is constructed, but maybe it comes from a file.
        Document d=e.getOwnerDocument();
        String uri=d.getDocumentURI();
        if(uri!=null && 0<uri.length())
        	location = "uri:(\"" +uri + "\")" + location;//better format?
        return location;
	}

	private String getLocation(Node source){
		if(source==null)
			return "null";//bad
		if(source instanceof Attr)
			return getLocation((Attr)source);
		if(source instanceof Text)
			return getLocation((Text)source);
		if(source instanceof Element)
			return getLocation((Element)source);
		return source.toString();//Uncontrolled
	}
	
	/** Evaluates data-var- attribute. 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ScriptException JS evaluation fails. Exception will have location.
	 * */
	private void renderVar(Element e, Attr dataVar)throws Exception{
        //Parse the variable
        String name=dataVar.getName();
        String var=name.substring(DATA_VAR.length(), name.length());
        String value=dataVar.getValue();

		Object x=evaluate(dataVar, value);//this is where javascript evaluation is
        boolean contains=contains(var);
        Object oldX=get(var);

        e.removeAttribute(name);
        put(var, x);//Put "var" -> x into the scope
        element(e);
        e.setAttribute(name, value);//Put data-var back, so that we leave document unchanged

        if(contains)put(var, oldX);else remove(var);
	}

	/** Evaluate data-if attribute.
	 * @return Should this element be rendered? 
	 * @throws ScriptException */
	private void renderIf(Element e, Attr dataIf)throws Exception{
		String v=dataIf.getValue();
		boolean b=evaluateBoolean(dataIf, v);
		if(b){
			e.removeAttributeNode(dataIf);//temporarily remove it
			element(e);
			e.setAttributeNode(dataIf);//We don't want to change the document
		}
	}
	
	/** Evaluates data-insert attribute. 
	 * 
	 * Discards the content of the element
	 * and fills it with the fragment from another file.
	 * 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ScriptException */
	@SuppressWarnings("unused")
	private void renderInsert(Element e, Attr dataInsert)throws Exception{
		//Get path of the fragment
		Document host=e.getOwnerDocument();
		String uri=host.getDocumentURI();
		String path=dataInsert.getValue();
 		path=resolve(uri, path);
 		
 		DocumentFragment fragment=getFragment(host, path);
		
 		//Render the new included stuff
		NodeList nodes=fragment.getChildNodes();
		nodes(nodes);
	}

	/** Evaluates data-substitute attribute. 
	 * The element e is just a dummy.
	 * Substitutes it with a document fragment from another file.
	 * 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ScriptException */
	private void renderSubstitute(Element e, Attr dataSubstitute)throws Exception{
		//Get path of the fragment
		Document host=e.getOwnerDocument();
		String uri=host.getDocumentURI();
		String path=dataSubstitute.getValue();
		path=resolve(uri, path);
		
		Timber.start("data-substitute=" + path);
		DocumentFragment fragment=getFragment(host, path);
		NodeList nodes=fragment.getChildNodes();
		nodes(nodes);
		Timber.stop("data-substitute=" + path);
	}
	
	/** Returns an absolute path from a base path and a path,
	 * which may be relative to base or may be absolute already.
	 * @param base If null, just returns path.
	 * @param path */
	private String resolve(String base, String path){
		if(base==null)return path;
		try {
			URI b = new URI(base);
			URI u=b.resolve(path);
			return u.toString();
		}
		catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}
	
	/** Gets a parsed document for a path.
	 * This implementation asks the HTMLServlet.
	 * Subclasses may override. 
	 * @param path The path is absolute. 
	 * @return the document
	 * @throws SAXException something wrong
	 * @throws IOException something wrong
	 * @throws FileNotFoundException Couldn't find the html file. */
	protected Document getDocument(String path) throws IOException, SAXException{
		return servlet.getDocument(path);
	}
	
	protected Document parse(Reader reader) throws IOException, SAXException{
		return servlet.parse(reader);
	}
	
	/** Gets a parsed document fragment for a path.
	 * 
	 * This implementation asks the HTMLServlet.
	 * Subclasses may override. 
	 * 
	 * @param host host document
	 * @param path The path is absolute. 
	 * @return the document fragment
	 * @throws SAXException something wrong
	 * @throws IOException something wrong  */
	protected DocumentFragment getFragment(Document host, String path) throws IOException, SAXException{
		//parse document from <fragment>source</fragment>
		//opt: avoid collect() by wrapping the reader
		Reader reader=getReader(path);
		String collected=collect(reader);
		String bracketed="<fragment>" + collected + "</fragment>";//fragile
		reader=new StringReader(bracketed);
		Document fake=parse(reader);
		
		DocumentFragment fragment=host.createDocumentFragment();
		//Node node=fragment.getFirstChild();//Equals to root? null
		Node root=fake.getElementsByTagName("fragment").item(0);//fragile?
		NodeList children=root.getChildNodes();
		for(int i=0; i<children.getLength(); i++){
			Node n=children.item(i);
			n=host.importNode(n, true);
			fragment.appendChild(n);
		}
		return fragment;
	}
	
	/** Does the scripting state have a variable? 
	 * @param var the variable to look for 
	 * @return Is the variable there? */
	protected boolean contains(String var){return bindings.containsKey(var);}
	
	/** Set a scripting variable. 
	 * @param var the variable identifier
	 * @param value the value to put in the variable */
	protected void put(String var, Object value){bindings.put(var, value);}
	
	/** What is the value of a scripting variable?
	 * Does not distinguish between null and no variable. 
	 * 
	 * For use outside scripting, may need unwrapping.
	 * @param var the variable identifier 
	 * @return the value in the variable */
	protected Object get(String var){return bindings.get(var);}
	
	/** Transforms a value from the scripting state to a value
	 * useful in Java. May transform undefined to null, and unwrap.
	 * May use details of the engine implementation.
	 * The method here just returns the same object.
	 * @param v the variable
	 * @return the value or null */
	protected Object unwrap(Object v){return v;}
	
	/** Removes the variable.
	 * @param var the variable */
	protected void remove(String var){bindings.remove(var);}

}

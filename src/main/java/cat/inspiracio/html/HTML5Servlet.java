/*
Copyright 2016 Alexander Bunkenburg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import java.io.IOException;
import java.io.Reader;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/** Like the HTMLServlet but uses the real html-parser which parses real HTML5. */
public class HTML5Servlet extends HTMLServlet {
    private static final long serialVersionUID = 3720401343819124152L;

    /** Parses real html5. 
	 * @see cat.inspiracio.html.HTMLServlet#parse(java.io.Reader) */
	@Override protected Document parse(Reader reader) throws IOException, SAXException{
		HTMLDocumentBuilder builder=new HTMLDocumentBuilder();
		InputSource source=new InputSource(reader);
		return builder.parse(source);
	}

}
